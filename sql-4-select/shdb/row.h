#pragma once

#include <cstdint>
#include <string>
#include <variant>
#include <vector>

namespace shdb {

struct Null
{
    bool operator==(const Null &other) const;
    bool operator!=(const Null &other) const;
};

using Value = std::variant<Null, bool, uint64_t, std::string>;
using Row = std::vector<Value>;
using RowIndex = int;

std::string to_string(const Row &row);

}    // namespace shdb

namespace std {

template<>
class hash<shdb::Null>
{
public:
    size_t operator()(const shdb::Null &null) const
    {
        return 0x12345678;
    }
};

template<class T>
inline void hash_combine(size_t &seed, const T &v)
{
    seed ^= std::hash<T>()(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

template<>
class hash<shdb::Row>
{
public:
    size_t operator()(const shdb::Row &row) const
    {
        size_t seed = 0;
        for (const auto &value : row) {
            hash_combine(seed, value);
        }
        return seed;
    }
};

}    // namespace std
