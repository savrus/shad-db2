#pragma once

#include <vector>
#include <string>

namespace shdb {

struct ColumnSchema
{
    std::string name;
};

using Schema = std::vector<ColumnSchema>;

}    // namespace shdb
