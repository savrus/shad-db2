#!/bin/bash

set -e
set -x

source usernames.sh

mkdir -p bins

for username in ${usernames[@]}
do
  cp ${username}/threads/main.cpp shad-db2/threads/main.cpp
  make -C shad-db2/threads main
  cp shad-db2/threads/main bins/${username}
done
