#!/bin/bash

set -e
set -x

source usernames.sh

ulimit -n 64000
make -C shad-db2/threads tester

port=1300

for run in `seq 1 4`
do
  for testid in `seq 1 6`
  do
    for username in ${usernames[@]}
    do
      skip="logs/${username}.${run}.${testid}.skip"
      stderr="logs/${username}.${run}.${testid}.stderr"
      stdout="logs/${username}.${run}.${testid}.stdout"
      fail="logs/${username}.${run}.${testid}.fail"
      failptrn="logs/${username}.?.${testid}.fail"
      
      if ./blacklist.py ${username} ${testid} || ls $failptrn;
      then
        touch $skip
      else
        if ls $stdout
        then
          echo "Run ${run}, test ${testid} for ${username} already done"
        else
          let port=port+1
          binary="bins/${username}"
          spec="shad-db2/threads/performance/test.${testid}.json"
          { timeout 600 shad-db2/threads/tester ${binary} ${port} $spec 2>&3 > $stdout || touch $fail; } 3>&1 | head -c 1024 > $stderr
          sleep 1
        fi
      fi
    done
  done
done
