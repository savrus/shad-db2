#!/bin/bash

user_commit=(
)

len=${#user_commit[@]}
let limit=len/2-1

for i in `seq 0 $limit`
do
  let u=2*i
  let v=2*i+1
  username=${user_commit[$u]}
  commit=${user_commit[$v]}
  cd $username
  git checkout $commit
  cd ..
done
