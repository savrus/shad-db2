#pragma once

#include "accessors.h"
#include "jit.h"
#include "schema.h"

namespace shdb {

struct Aggregate
{
    virtual ~Aggregate() = default;

    Jit &jit;
    Type type;

    Aggregate(Jit &jit, Type type);
    virtual size_t get_state_size() = 0;
    virtual void init(llvm::Value *state_ptr) = 0;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) = 0;
    virtual JitValue finalize(llvm::Value *state_ptr) = 0;
};

struct Sum : public Aggregate
{
    Sum(Jit &jit, Type type);
    virtual size_t get_state_size() override;
    virtual void init(llvm::Value *state_ptr) override;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) override;
    virtual JitValue finalize(llvm::Value *state_ptr) override;
};

struct Min : public Aggregate
{
    Min(Jit &jit, Type type);
    virtual size_t get_state_size() override;
    virtual void init(llvm::Value *state_ptr) override;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) override;
    virtual JitValue finalize(llvm::Value *state_ptr) override;
};

struct Max : public Aggregate
{
    Max(Jit &jit, Type type);
    virtual size_t get_state_size() override;
    virtual void init(llvm::Value *state_ptr) override;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) override;
    virtual JitValue finalize(llvm::Value *state_ptr) override;
};

struct Avg : public Aggregate
{
    Avg(Jit &jit, Type type);
    virtual size_t get_state_size() override;
    virtual void init(llvm::Value *state_ptr) override;
    virtual void update(llvm::Value *state_ptr, const JitValue &value) override;
    virtual JitValue finalize(llvm::Value *state_ptr) override;
};

std::unique_ptr<Aggregate> create_aggregate(Jit &jit, const std::string &name, Type type);

}    // namespace shdb
