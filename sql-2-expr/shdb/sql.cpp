#include "sql.h"

#include "accessors.h"
#include "ast.h"
#include "flow.h"
#include "gear.h"
#include "generator.h"
#include "grouper.h"
#include "jit.h"
#include "lexer.h"
#include "parser.hpp"
#include "row.h"

#include <cassert>
#include <iostream>
#include <string>
#include <unordered_set>

namespace shdb {

class SqlImpl : public Sql
{
public:
    SqlImpl(std::shared_ptr<Database> db)
    {}

    virtual Rowset execute(const std::string &query) override
    {
        // Your code goes here
        std::abort();
    }
};

std::shared_ptr<Sql> create_sql(std::shared_ptr<Database> db)
{
    return std::make_shared<SqlImpl>(std::move(db));
}

}    // namespace shdb
