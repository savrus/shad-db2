#pragma once

// Message is a typed unit of communication.
//
// Multiple message types exist. These types are differentiated by the MessageType
// enum below.
// Some message types come with type-specific payload.

// Every message has a unique id. The id does not change when the message is retried.
//
// Some message types are "reply" messages for a given "request" message. Reply messages
// always carry a request_id field, id of the request message.
//
// Every message is associated with a transaction. All message types carry field
// txid, id for the transaction that message belongs to.
//
// Every message type is sent and received by nodes acting in a certain role.
// Four roles exist:
//
// * Client, a node that starts a transactions, performs reads or writes, and finally
//   decides to commit or rollback the transaction.
// * Server, a node that hosts an interval of key space. Server serves requests from
//   the client.
// * Coordinator. In a multi-node transaction, one server node that orchestrates the
//   commit of the transaction.
// * Participant. In a multi-node transaction, all non-coordinator server nodes.
//
// Every message type is strictly tied to source and destination node roles. These roles
// are specified in comments for MessageType enum, see below.

#include <string>
#include <variant>
#include <vector>

#include "message_specific.h"
#include "types.h"

enum MessageType {
  MSG_UNDEFINED,

  // client -> server. The client requests start of a transaction, and provides
  // txid for the new transaction.
  MSG_START,

  // server -> client. Server acknowledges start of a transaction, and provides
  // read_timestamp, a timestamp for all reads within the transaction.
  MSG_START_ACK,

  // client -> server.
  // Read a specific key within the transaction.
  MSG_GET,

  // server -> client.
  // Respond with a value for the key.
  MSG_GET_REPLY,

  // client -> server.
  // Write a value for the given key.
  MSG_PUT,

  // server -> client.
  // Acknowledge writing a key.
  MSG_PUT_REPLY,

  // client -> coordinator server.
  // Ask the coordinator to commit the transaction.
  // Provide ActorId for all other participants in the transaction.
  // The coordinator is responsible for performing 2PC across all participants.
  MSG_COMMIT,

  // This message serves two similar purposes:
  //
  // coordinator server -> client.
  // After the client requested Commit of a transaction, inform the client
  // that it has been successfully committed.
  //
  // coordinator server -> participant server.
  // Upon learning from all non-coordinator participants that the transaction
  // has been prepared, inform a participant that it moved into the Committed
  // state.
  MSG_COMMIT_ACK,

  // client -> server.
  // Request a Rollback of the transaction.
  MSG_ROLLBACK,

  // server -> client.
  // After receiving a Rollback request from the client, inform the client
  // that the transaction has been rolled back.
  //
  // participant -> coordinator. After receiving Rolled Back By Server,
  // the participant sucessfully rolled back a conflicting transaction.
  MSG_ROLLBACK_ACK,

  // This message serves two similar purposes:
  //
  // server -> client.
  // inform client that server rolled back transaction due to
  // conflict with other transaction.
  //
  // coordinator server -> participant server.
  // inform a parcicipant that a conflict has been detected and transaction
  // was rolled back.
  MSG_ROLLED_BACK_BY_SERVER,

  // coordinator server -> participant server
  // Prepare to commit, by checking conflicts. Coordinator provides commit_timestamp
  // with this message.
  MSG_PREPARE,

  // participant server -> coordinator server
  // In response to the Prepare message, inform the coordinator that 
  // parcipipants successfully prepared, and no conflicts have been found.
  MSG_PREPARE_ACK,

  // participant server -> coordinator server
  // In response to the Prepare message, inform the coordinator that
  // prepare failed because a conflit was discovered.
  MSG_CONFLICT,
};

std::string format_message_type(MessageType type);

struct Message {
  MessageType type{MSG_UNDEFINED};
  ActorId source{-1};
  ActorId destination{-1};

  // Globally unique message identifier. Can be used to differentiate requests
  // when processing the response.
  //
  // Does not change when request is retried.
  int64_t id;

  std::variant<MessageStartPayload, MessageStartAckPayload, MessageGetPayload,
               MessageGetReplyPayload, MessagePutPayload,
               MessagePutReplyPayload, MessageRolledBackByServerPayload,
               MessagePreparePayload, MessagePrepareAckPayload,
               MessageConflictPayload, MessageCommitPayload,
               MessageCommitAckPayload, MessageRollbackPayload,
               MessageRollbackAckPayload>
      payload;

  template <class T> const T &get() const { return std::get<T>(this->payload); }

  template <class T> T &get() { return std::get<T>(this->payload); }
};

// Factory functions for message objects.
Message CreateMessageStart(ActorId source, ActorId destination,
                           TransactionId txid = UNDEFINED_TRANSACTION_ID,
                           Timestamp read_timestamp = UNDEFINED_TIMESTAMP);

Message CreateMessageStartAck(ActorId source, ActorId destination,
                              TransactionId txid, Timestamp read_timestamp);
Message CreateMessageGet(ActorId source, ActorId destination,
                         TransactionId txid, Key key);
Message CreateMessagePut(ActorId source, ActorId destination,
                         TransactionId txid, Key key, Value value);
Message CreateMessageGetReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id, Value value);
Message CreateMessagePutReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id);
Message CreateMessageRolledBackByServer(ActorId source, ActorId destination,
                                        TransactionId txid,
                                        int64_t conflict_txid);
Message CreateMessageCommit(ActorId source, ActorId destination,
                            TransactionId txid,
                            std::vector<ActorId> participants);
Message CreateMessageCommitAck(ActorId source, ActorId destination,
                               TransactionId txid, Timestamp commit_timestamp);
Message CreateMessageRollback(ActorId source, ActorId destination,
                              TransactionId txid);
Message CreateMessageRollbackAck(ActorId source, ActorId destination,
                                 TransactionId txid);

// Messages Prepare, PrepareAck, ConflictAck are only used in server-to-server
// communication within the two-phase-commit protocol.
Message CreateMessagePrepare(ActorId source, ActorId destination,
                             TransactionId txid, Timestamp commit_timestamp);
Message CreateMessagePrepareAck(ActorId source, ActorId destination,
                                TransactionId txid);
Message CreateMessageConflict(ActorId source, ActorId destination,
                              TransactionId txid, TransactionId conflict_txid);
