#pragma once

// ClientTransaction class executes one transaction, according to
// provided ClientTransactionSpec. It sends and receives messages on behalf
// of the client.
//
// Every transaction may read and write keys that belong to different server
// nodes. For every server node that belongs to a transaction,
// ClientTransaction maintains a separate ClientTransactionParticipant object
// that incapsulates state and logic related to that node.
//
// Among all server nodes, a random node is selected to be a Coordinator node.
// The Coordinator is a first node where ClientTransactions sends the Start
// message, with undefined read_timestamp.
// The Coordinator will reply with StartAck message, providing read_timestamp.
// That timestamp will be used for Get queries in the transaction.
//
// When the transaction is about to be committed, ClientTransaction sends a
// Commit message to the coordinator server.
// The list of non-coordinator participants is included in that message.
//
// If no other server nodes participate in the transaction, the coordinator
// independently checks for conflicts, and decides whether to commit or rollback
// the transaction.
// If other participants exist, the coordinator executes the 2-phase-commit
// protocol.
//
// Note that the role coordinator is only limited to selecting read_timestamp and
// orchestrating transaction commits.
// 
// When reading and writing key/values, the client interacts with server nodes
// directly.
// Also, client-initiated rollback does not involve the coordinator - the client
// simply sends a Rollback message to every server node that participate in the
// transaction.

#include <cstdint>
#include <memory>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "client_transaction_spec.h"
#include "client_transaction_participant.h"
#include "client_transaction_results.h"
#include "discovery.h"
#include "message.h"
#include "retrier.h"
#include "types.h"

enum class ClientTransactionState {
  // Initial state, before Start message is sent.
  NOT_STARTED,

  // Start message has been sent, and txid assigned.
  START_SENT,

  // Coordinator replied with StartAck and provided read_timestamp.
  OPEN,

  // Client started to roll back this transaction, by sending a Rollback message
  // to all servers that participate in it.
  ROLLBACK_SENT,

  // All participants confirmed the rollback.
  ROLLED_BACK,

  // The coordinator informed the client that the transaction has been rolled back
  // on the server side, because one server node detected a conflict with
  // another transaction.
  ROLLED_BACK_BY_SERVER,

  // Client requested commiting transaction from the coordinator.
  COMMIT_SENT,

  // The coordinator successfully committed the transaction, and informed
  // the client.
  COMMITTED,
};

std::string format_client_transaction_state(ClientTransactionState state);

class ClientTransaction {
public:
  ClientTransaction(ActorId self, ClientTransactionSpec spec,
                    const Discovery *discovery, IRetrier *rt);

  void tick(Timestamp ts, const std::vector<Message> &messages,
            std::vector<Message> *msg_out);

  ClientTransactionState state() const { return state_; }
  ClientTransactionResults export_results() const;

  ActorId id() const { return self_; }
  TransactionId get_id() const;

private:
  enum class ParticipantState { NOT_STARTED, START_SENT, STARTED };

  enum class RequestState { NOT_STARTED, STARTED, COMPLETED };

  struct GetState {
    RequestState request_state{RequestState::NOT_STARTED};
    IRetrier::Handle rt_handle{IRetrier::UNDEFINED_HANDLE};
    Value value;
  };

  struct PutState {
    RequestState request_state{RequestState::NOT_STARTED};
    IRetrier::Handle rt_handle{IRetrier::UNDEFINED_HANDLE};
  };

  // Process replies, in corresponding states.
  void process_replies_not_started(const std::vector<Message> &messages);
  void process_replies_start_sent(const std::vector<Message> &messages);
  void process_replies_open(const std::vector<Message> &messages);
  void process_replies_commit_sent(const std::vector<Message> &messages);
  void process_replies_rollback_sent(const std::vector<Message> &messages);
  void process_replies_commited(const std::vector<Message> &messages);
  void process_replies_rolled_back(const std::vector<Message> &messages);
  void
  process_replies_rolled_back_by_server(const std::vector<Message> &messages);

  void handle_rolled_back_by_server(const Message &);

  std::tuple<int, int> completed_requests() const;

  void maybe_init_participant(ActorId id, Timestamp ts);
  void report_unexpected_msg(const Message &);

  std::unordered_map<ActorId, std::unique_ptr<ClientTransactionParticipant>> p_;

  std::vector<ActorId> get_participants_except_coordinator() const;

  const ActorId self_;
  const ClientTransactionSpec spec_;
  const Discovery *const discovery_;
  IRetrier *const rt_;

  TransactionId txid_{UNDEFINED_TRANSACTION_ID};
  ActorId coordinator_{UNDEFINED_ACTOR_ID};
  ClientTransactionState state_{ClientTransactionState::NOT_STARTED};
  TransactionId conflict_txid_{UNDEFINED_TRANSACTION_ID};
  IRetrier::Handle commit_handle_{IRetrier::UNDEFINED_HANDLE};

  std::unordered_map<ActorId, IRetrier::Handle> rollback_handle_;

  Timestamp read_timestamp_{UNDEFINED_TIMESTAMP};
  Timestamp commit_timestamp_{UNDEFINED_TIMESTAMP};

  size_t next_get_{0};
  size_t next_put_{0};
};
