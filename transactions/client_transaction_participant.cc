#include "client_transaction_participant.h"
#include "log.h"

ClientTransactionParticipant::ClientTransactionParticipant(ActorId client,
                                                           ActorId actor,
                                                           IRetrier *rt)
    : client_(client), target_(actor), rt_(rt) {}

void ClientTransactionParticipant::start(Timestamp ts) {
  Message msg = CreateMessageStart(client_, target_);
  txid_ = msg.get<MessageStartPayload>().txid;
  start_handle_ = rt_->schedule(ts, msg);
  state_ = START_SENT;
}

void ClientTransactionParticipant::start_at_timestamp(
    Timestamp ts, TransactionId txid, Timestamp read_timestamp) {
  txid_ = txid;
  read_timestamp_ = read_timestamp;
  Message msg = CreateMessageStart(client_, target_, txid, read_timestamp);
  start_handle_ = rt_->schedule(ts, msg);
  state_ = START_SENT;
}

void ClientTransactionParticipant::issue_get(Key key) {
  get_state_.push_back(RequestState{RequestState::NOT_STARTED,
                                    IRetrier::UNDEFINED_HANDLE, key, 0});
}

void ClientTransactionParticipant::issue_put(Key key, Value value) {
  put_state_.push_back(RequestState{RequestState::NOT_STARTED,
                                    IRetrier::UNDEFINED_HANDLE, key, value});
}

void ClientTransactionParticipant::process_incoming(
    Timestamp ts, const std::vector<Message> &messages) {
  switch (state_) {
  case NOT_STARTED: {
    for (const auto &msg : messages) {
      report_unexpected_msg(msg);
    }
    break;
  }
  case START_SENT: {
    process_replies_start_sent(messages);
    break;
  }
  case OPEN: {
    process_replies_open(messages);
    break;
  }
  }
}

void ClientTransactionParticipant::maybe_issue_requests(Timestamp ts) {
  if (state_ != OPEN)
    return;

  for (; next_get_ < get_state_.size(); ++next_get_) {
    get_state_[next_get_].request_state = RequestState::STARTED;
    auto msg =
        CreateMessageGet(client_, target_, txid_, get_state_[next_get_].key);
    get_state_[next_get_].rt_handle = rt_->schedule(ts, msg);
    get_request_[msg.id] = next_get_;
  }
  for (; next_put_ < put_state_.size(); ++next_put_) {
    put_state_[next_put_].request_state = RequestState::STARTED;
    auto msg =
        CreateMessagePut(client_, target_, txid_, put_state_[next_put_].key,
                         put_state_[next_put_].value);
    put_state_[next_put_].rt_handle = rt_->schedule(ts, msg);
    put_request_[msg.id] = next_put_;
  }
}

void ClientTransactionParticipant::process_replies_start_sent(
    const std::vector<Message> &messages) {
  Timestamp rts = -1;
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_START_ACK: {
      rts = msg.get<MessageStartAckPayload>().read_timestamp;
      rt_->cancel(start_handle_);
      break;
    }
    default: {
      report_unexpected_msg(msg);
      break;
    }
    }
  }

  if (rts == -1) {
    return;
  }

  if (read_timestamp_ == UNDEFINED_TIMESTAMP) {
    read_timestamp_ = rts;
  }

  state_ = OPEN;
}

void ClientTransactionParticipant::report_unexpected_msg(const Message &msg) {
  LOG_ERROR << "[tx " << txid_ << "] Node " << msg.source
            << " returns unexpected message type " << msg.type
            << " for client transaction in state " << (int)state_;
}

void ClientTransactionParticipant::process_replies_open(
    const std::vector<Message> &messages) {
  for (const auto &msg : messages) {
    switch (msg.type) {
    case MSG_START_ACK: {
      // Ignore duplicate.
      break;
    }
    case MSG_GET_REPLY: {
      int64_t req_id = msg.get<MessageGetReplyPayload>().request_id;
      if (get_request_.count(req_id) > 0) {
        auto &state = get_state_[get_request_[req_id]];
        if (state.request_state == RequestState::STARTED) {
          state.request_state = RequestState::COMPLETED;
          state.value = msg.get<MessageGetReplyPayload>().value;
          rt_->cancel(state.rt_handle);
          ++completed_gets_;
        }
      } else {
        LOG_ERROR << "[tx " << txid_ << "] Node " << msg.source
                  << " returns Get Reply id " << msg.id
                  << " that does not match any previous requests";
      }
      break;
    }
    case MSG_PUT_REPLY: {
      int64_t req_id = msg.get<MessagePutReplyPayload>().request_id;
      if (put_request_.count(req_id) > 0) {
        auto &state = put_state_[put_request_[req_id]];
        if (state.request_state == RequestState::STARTED) {
          state.request_state = RequestState::COMPLETED;
          rt_->cancel(state.rt_handle);
          ++completed_puts_;
        }
      } else {
        LOG_ERROR << "[tx " << txid_ << "] Node " << msg.source
                  << " returns Put Reply id " << msg.id
                  << "that does not match any previous requests";
      }
      break;
    }
    default: {
      report_unexpected_msg(msg);
    }
    }
  }
}

void ClientTransactionParticipant::export_results(
    std::vector<ClientTransactionResults::KeyValue> *gets,
    std::vector<ClientTransactionResults::KeyValue> *puts) const {
  for (const auto &rs : get_state_) {
    if (rs.request_state != RequestState::COMPLETED)
      continue;
    gets->push_back({rs.key, rs.value});
  }
  for (const auto &rs : put_state_) {
    if (rs.request_state != RequestState::COMPLETED)
      continue;
    puts->push_back({rs.key, rs.value});
  }
}
