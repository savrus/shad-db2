#include <shdb/db.h>
#include <shdb/sql.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <cassert>
#include <chrono>
#include <iostream>
#include <sstream>

auto fixed_schema = std::make_shared<shdb::Schema>(shdb::Schema{
    {"id", shdb::Type::uint64},
    {"name", shdb::Type::varchar, 1024},
    {"age", shdb::Type::uint64},
    {"graduated", shdb::Type::boolean}});
auto flexible_schema = std::make_shared<shdb::Schema>(shdb::Schema{
    {"id", shdb::Type::uint64},
    {"name", shdb::Type::string},
    {"age", shdb::Type::uint64},
    {"graduated", shdb::Type::boolean}});

std::shared_ptr<shdb::Database> create_database()
{
    auto db = shdb::connect();
    if (db->check_table_exists("test_table")) {
        db->drop_table("test_table");
    }
    db->create_table("test_table", fixed_schema);
    return db;
}

void test_ddl()
{
    auto db = create_database();
    assert(db->check_table_exists("test_table"));

    auto sql = shdb::create_sql(db);
    sql->execute("DROP TABLE test_table");
    assert(!db->check_table_exists("test_table"));

    sql->execute("CREATE TABLE test_table (id uint64, name string, nick varchar(44), flag boolean)");
    assert(db->check_table_exists("test_table"));
    auto schema = db->find_table_schema("test_table");
    assert(schema);

    auto &columns = *schema;
    assert(columns.size() == 4);
    assert(columns[0].name == "id" && columns[0].type == shdb::Type::uint64 && columns[0].length == 0);
    assert(columns[1].name == "name" && columns[1].type == shdb::Type::string && columns[1].length == 0);
    assert(columns[2].name == "nick" && columns[2].type == shdb::Type::varchar && columns[2].length == 44);
    assert(columns[3].name == "flag" && columns[3].type == shdb::Type::boolean && columns[3].length == 0);

    sql->execute("DROP TABLE test_table");
    assert(!db->check_table_exists("test_table"));

    std::cout << "Test ddl passed" << std::endl;
}

void test_expression()
{
    auto sql = shdb::create_sql(nullptr);

    auto check = [&](const std::string &query, const shdb::Row &expected) {
        auto result = sql->execute(query);
        assert(result.rows.size() == 1 && *result.rows[0] == expected);
    };

    check("SELECT 11", {static_cast<uint64_t>(11)});
    check("SELECT 11+11", {static_cast<uint64_t>(22)});
    check("SELECT 2*2", {static_cast<uint64_t>(4)});
    check("SELECT 1 > 0", {true});
    check("SELECT (50-30)*2 <= 1*2*3*4", {false});
    check("SELECT \"Hi\"", {std::string("Hi")});
    check(
        "SELECT \"Mike\", \"Bob\", 1+2, 1>0",
        {std::string("Mike"), std::string("Bob"), static_cast<uint64_t>(3), true});

    std::cout << "Test expression passed" << std::endl;
}

void populate(const std::shared_ptr<shdb::Sql> &sql)
{
    sql->execute("DROP TABLE test_table");
    sql->execute("CREATE TABLE test_table (id uint64, age uint64, name string, girl boolean)");
    sql->execute("INSERT INTO test_table VALUES (0, 10+10, \"Ann\", 1>0)");
    sql->execute("INSERT INTO test_table VALUES (1, 10+10+1, \"Bob\", 1<0)");
    sql->execute("INSERT INTO test_table VALUES (2, 10+9, \"Sara\", 1>0)");

    sql->execute("DROP TABLE test_orders");
    sql->execute("CREATE TABLE test_orders (id uint64, order string, price uint64)");
    sql->execute("INSERT INTO test_orders VALUES (0, \"pizza\", 99)");
    sql->execute("INSERT INTO test_orders VALUES (0, \"cola\", 49)");
    sql->execute("INSERT INTO test_orders VALUES (2, \"burger\", 599)");
}

void test_insert()
{
    auto db = create_database();
    auto sql = shdb::create_sql(db);
    populate(sql);

    auto rows = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    size_t index = 0;
    auto table = db->get_table("test_table");

    for (auto* row : table->rows) {
        assert(*row == rows[index]);
        ++index;
    }

    assert(index == rows.size());

    std::cout << "Test insert passed" << std::endl;
}

void assert_rows_equal(const std::vector<shdb::Row> &rows, const shdb::Rowset &rowset)
{
    assert(rows.size() == rowset.rows.size());
    for (size_t index = 0; index < rows.size(); ++index) {
        assert(rows[index] == *rowset.rows[index]);
    }
};

void test_select()
{
    auto db = create_database();
    auto sql = shdb::create_sql(db);
    populate(sql);

    auto rows1 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};
    auto result1 = sql->execute("SELECT * FROM test_table");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};
    auto result2 = sql->execute("SELECT * FROM test_table WHERE age <= 20");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(1), std::string("Ann"), true},
        {static_cast<uint64_t>(2), std::string("Bob"), true},
        {static_cast<uint64_t>(3), std::string("Sara"), false}};
    auto result3 = sql->execute("SELECT id+1, name, id < 2 FROM test_table");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{
        {std::string("Bob"), static_cast<uint64_t>(2)}, {std::string("Sara"), static_cast<uint64_t>(4)}};
    auto result4 = sql->execute("SELECT name, id*2 FROM test_table WHERE id > 0");
    assert_rows_equal(rows4, result4);

    std::cout << "Test select passed" << std::endl;
}

void test_order()
{
    auto db = create_database();
    auto sql = shdb::create_sql(db);
    populate(sql);

    auto rows1 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true},
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false}};

    auto result1 = sql->execute("SELECT * FROM test_table ORDER BY age");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    auto result2 = sql->execute("SELECT * FROM test_table ORDER BY age DESC");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    auto result3 = sql->execute("SELECT * FROM test_table ORDER BY name");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true}};

    auto result4 = sql->execute("SELECT * FROM test_table ORDER BY name DESC");
    assert_rows_equal(rows4, result4);

    auto rows5 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    auto result5 = sql->execute("SELECT * FROM test_table ORDER BY name, age");
    assert_rows_equal(rows5, result5);

    auto rows6 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true}};

    auto result6 = sql->execute("SELECT * FROM test_table ORDER BY name DESC, age");
    assert_rows_equal(rows6, result6);

    auto rows7 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true}};

    auto result7 = sql->execute("SELECT * FROM test_table ORDER BY name, age DESC");
    assert_rows_equal(rows7, result7);

    auto rows8 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(2), static_cast<uint64_t>(19), std::string("Sara"), true},
        {static_cast<uint64_t>(1), static_cast<uint64_t>(21), std::string("Bob"), false},
        {static_cast<uint64_t>(0), static_cast<uint64_t>(20), std::string("Ann"), true}};

    auto result8 = sql->execute("SELECT * FROM test_table ORDER BY age - id * 2");
    assert_rows_equal(rows8, result8);

    std::cout << "Test order passed" << std::endl;
}

void test_join()
{
    auto db = create_database();
    auto sql = shdb::create_sql(db);
    populate(sql);

    auto rows1 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0),
         static_cast<uint64_t>(20),
         std::string("Ann"),
         true,
         std::string("pizza"),
         static_cast<uint64_t>(99)},
        {static_cast<uint64_t>(0),
         static_cast<uint64_t>(20),
         std::string("Ann"),
         true,
         std::string("cola"),
         static_cast<uint64_t>(49)},
        {static_cast<uint64_t>(2),
         static_cast<uint64_t>(19),
         std::string("Sara"),
         true,
         std::string("burger"),
         static_cast<uint64_t>(599)}};

    auto result1 = sql->execute("SELECT * FROM test_table, test_orders");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(0),
         static_cast<uint64_t>(20),
         std::string("Ann"),
         true,
         std::string("pizza"),
         static_cast<uint64_t>(99)},
        {static_cast<uint64_t>(2),
         static_cast<uint64_t>(19),
         std::string("Sara"),
         true,
         std::string("burger"),
         static_cast<uint64_t>(599)}};

    auto result2 = sql->execute("SELECT * FROM test_table, test_orders WHERE price > 50");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(99)},
        {std::string("Ann"), static_cast<uint64_t>(49)},
        {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result3 = sql->execute("SELECT name, price FROM test_table, test_orders");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(99)}, {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result4 = sql->execute("SELECT name, price FROM test_table, test_orders WHERE price > 50");
    assert_rows_equal(rows4, result4);

    auto rows5 = std::vector<shdb::Row>{
        {std::string("Sara"), static_cast<uint64_t>(599)}, {std::string("Ann"), static_cast<uint64_t>(99)}};

    auto result5 = sql->execute("SELECT name, price FROM test_table, test_orders WHERE price > 50 ORDER BY name DESC");
    assert_rows_equal(rows5, result5);

    auto rows6 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(49)},
        {std::string("Ann"), static_cast<uint64_t>(99)},
        {std::string("Sara"), static_cast<uint64_t>(599)}};

    auto result6 = sql->execute("SELECT name, price FROM test_table, test_orders ORDER BY price");
    assert_rows_equal(rows6, result6);

    auto rows7 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(2),
         static_cast<uint64_t>(19),
         std::string("Sara"),
         true,
         std::string("burger"),
         static_cast<uint64_t>(599)},
        {static_cast<uint64_t>(0),
         static_cast<uint64_t>(20),
         std::string("Ann"),
         true,
         std::string("cola"),
         static_cast<uint64_t>(49)},
        {static_cast<uint64_t>(0),
         static_cast<uint64_t>(20),
         std::string("Ann"),
         true,
         std::string("pizza"),
         static_cast<uint64_t>(99)}};

    auto result7 = sql->execute("SELECT * FROM test_table, test_orders ORDER BY name DESC, price");
    assert_rows_equal(rows7, result7);

    std::cout << "Test join passed" << std::endl;
}

void populate_group(const std::shared_ptr<shdb::Sql> &sql)
{
    sql->execute("DROP TABLE test_table");
    sql->execute("CREATE TABLE test_table (id uint64, age uint64, name string, girl boolean)");
    sql->execute("INSERT INTO test_table VALUES (0, 20, \"Ann\", 1>0)");
    sql->execute("INSERT INTO test_table VALUES (1, 21, \"Bob\", 1<0)");
    sql->execute("INSERT INTO test_table VALUES (2, 19, \"Sara\", 1>0)");
    sql->execute("INSERT INTO test_table VALUES (3, 20, \"Ann\", 1>0)");
    sql->execute("INSERT INTO test_table VALUES (4, 15, \"Bob\", 1<0)");
    sql->execute("INSERT INTO test_table VALUES (5, 15, \"Bob\", 1<0)");
}

void test_group()
{
    auto db = create_database();
    auto sql = shdb::create_sql(db);
    populate_group(sql);

    auto rows1 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(15), static_cast<uint64_t>(2)},
        {static_cast<uint64_t>(19), static_cast<uint64_t>(1)},
        {static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
        {static_cast<uint64_t>(21), static_cast<uint64_t>(1)}};

    auto result1 = sql->execute("SELECT age, sum(1) FROM test_table GROUP BY age ORDER BY age");
    assert_rows_equal(rows1, result1);

    auto rows2 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(40)},
        {std::string("Bob"), static_cast<uint64_t>(51)},
        {std::string("Sara"), static_cast<uint64_t>(19)}};

    auto result2 = sql->execute("SELECT name, sum(age) FROM test_table GROUP BY name ORDER BY name");
    assert_rows_equal(rows2, result2);

    auto rows3 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(2)},
        {std::string("Bob"), static_cast<uint64_t>(21), static_cast<uint64_t>(1)},
        {std::string("Sara"), static_cast<uint64_t>(19), static_cast<uint64_t>(1)}};

    auto result3 = sql->execute("SELECT name, age, sum(1) FROM test_table GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows3, result3);

    auto rows4 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20)},
        {std::string("Bob"), static_cast<uint64_t>(15)},
        {std::string("Bob"), static_cast<uint64_t>(21)},
        {std::string("Sara"), static_cast<uint64_t>(19)}};

    auto result4 = sql->execute("SELECT * FROM test_table GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows4, result4);

    auto rows5 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(20), static_cast<uint64_t>(20)},
        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(21), static_cast<uint64_t>(17)},
        {std::string("Sara"), static_cast<uint64_t>(19), static_cast<uint64_t>(19), static_cast<uint64_t>(19)}};

    auto result5 = sql->execute("SELECT name, min(age), max(age), avg(age) FROM test_table GROUP BY name ORDER BY name");
    assert_rows_equal(rows5, result5);

    auto rows6 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
        {std::string("Bob"), static_cast<uint64_t>(21), static_cast<uint64_t>(1)}};

    auto result6 =
        sql->execute("SELECT name, age, sum(1) FROM test_table WHERE age >=20 GROUP BY name, age ORDER BY name, age");
    assert_rows_equal(rows6, result6);

    auto rows7 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(20), static_cast<uint64_t>(2)},
        {std::string("Bob"), static_cast<uint64_t>(15), static_cast<uint64_t>(2)}};

    auto result7 =
        sql->execute("SELECT name, age, sum(1) FROM test_table GROUP BY name, age HAVING sum(1) > 1 ORDER BY name, age");
    assert_rows_equal(rows7, result7);

    auto rows8 = std::vector<shdb::Row>{{std::string("Bob"), static_cast<uint64_t>(66)}};

    auto result8 =
        sql->execute("SELECT name, min(age) + sum(age) FROM test_table GROUP BY name HAVING max(age) - avg(age) > 0");
    assert_rows_equal(rows8, result8);

    auto rows9 = std::vector<shdb::Row>{
        {std::string("Bob"), static_cast<uint64_t>(15)},
        {std::string("Sara"), static_cast<uint64_t>(19)},
        {std::string("Ann"), static_cast<uint64_t>(20)}};

    auto result9 = sql->execute("SELECT name, min(age) FROM test_table GROUP BY name ORDER BY min(age)");
    assert_rows_equal(rows9, result9);

    auto rows10 = std::vector<shdb::Row>{
        {static_cast<uint64_t>(19), static_cast<uint64_t>(15), static_cast<uint64_t>(15)},
        {static_cast<uint64_t>(20), static_cast<uint64_t>(15), static_cast<uint64_t>(20)},
        {static_cast<uint64_t>(21), static_cast<uint64_t>(19), static_cast<uint64_t>(19)},
        {static_cast<uint64_t>(22), static_cast<uint64_t>(21), static_cast<uint64_t>(21)},
        {static_cast<uint64_t>(23), static_cast<uint64_t>(20), static_cast<uint64_t>(20)}};

    auto result10 =
        sql->execute("SELECT age + id, min(age), max(age) FROM test_table GROUP BY age + id ORDER BY age + id");
    assert_rows_equal(rows10, result10);

    auto rows11 = std::vector<shdb::Row>{
        {std::string("Ann"), static_cast<uint64_t>(23)},
        {std::string("Bob"), static_cast<uint64_t>(22)},
        {std::string("Sara"), static_cast<uint64_t>(21)}};

    auto result11 = sql->execute("SELECT name, max(age + id) FROM test_table GROUP BY name ORDER BY name");
    assert_rows_equal(rows11, result11);

    std::cout << "Test group passed" << std::endl;
}

void cmd()
{
    auto db = shdb::connect();
    auto sql = shdb::create_sql(db);

    while (!std::cin.eof()) {
        std::cout << "shdb> " << std::flush;
        std::string line;
        std::getline(std::cin, line);
        if (!line.empty()) {
            try {
                auto rowset = sql->execute(line);
                for (auto *row : rowset.rows) {
                    std::cout << to_string(*row) << std::endl;
                }
            } catch (char const *ex) {
                std::cout << "Error: " << ex << std::endl;
            }
        }
    }
}

int main(int argc, char *argv[])
{
    test_ddl();
    test_expression();
    test_insert();
    test_select();

    test_order();
    test_join();
    test_group();

    // cmd();
}
