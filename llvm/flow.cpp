#include "flow.h"

namespace shdb {

using llvm::Value;
using llvm::CmpInst;

For::For(Jit &jit, Value *&index, Value* start, Value* end, std::vector<Value**> variables)
    : jit(jit), index(index), variables(std::move(variables))
{
    increment = jit.create_constant(start->getType()->getIntegerBitWidth(), 1);

    auto *initbb = jit.builder.GetInsertBlock();
    auto *function = initbb->getParent();
    index = start;

    loopbb = jit.create_bb("loop_cond", function);
    jit.builder.CreateBr(loopbb);
    jit.builder.SetInsertPoint(loopbb);
    iterator = jit.builder.CreatePHI(start->getType(), 2, "loop_iterator");
    iterator->addIncoming(index, initbb);
    index = iterator;

    for (auto** var : For::variables) {
        phis.push_back(jit.builder.CreatePHI((*var)->getType(), 2));
        phis.back()->addIncoming(*var, initbb);
        *var = phis.back();
    }

    auto *bodybb = jit.create_bb("loop_body", function);
    outbb = jit.create_bb("loop_out", function);

    auto *cond = jit.builder.CreateICmp(CmpInst::ICMP_SLT, iterator, end);
    jit.builder.CreateCondBr(cond, bodybb, outbb);
    jit.builder.SetInsertPoint(bodybb);
}

For::~For()
{
    auto *incrementbb = jit.builder.GetInsertBlock();
    auto *next = jit.builder.CreateAdd(iterator, increment);
    iterator->addIncoming(next, incrementbb);

    for (int index = 0; index < std::ssize(variables); ++index) {
        phis[index]->addIncoming(*variables[index], incrementbb);
        *variables[index] = phis[index];
    }

    jit.builder.CreateBr(loopbb);
    jit.builder.SetInsertPoint(outbb);
}


If::If(Jit &jit, Value *cond, std::vector<Value**> variables)
    : jit(jit), variables(std::move(variables))
{
    auto *initbb = jit.builder.GetInsertBlock();
    auto *function = initbb->getParent();

    auto *thenbb = jit.create_bb("if_then", function);
    nextbb = jit.create_bb("if_next", function);
    jit.builder.CreateCondBr(cond, thenbb, nextbb);
    jit.builder.SetInsertPoint(thenbb);

    previousbb = initbb;
    for (auto** var : If::variables) {
        previous.push_back(*var);
    }
}

If::~If()
{
    auto *incomingbb = jit.builder.GetInsertBlock();

    jit.builder.CreateBr(nextbb);
    jit.builder.SetInsertPoint(nextbb);

    for (int index = 0; index < std::ssize(variables); ++index) {
        auto **var = variables[index];
        auto* phi = jit.builder.CreatePHI((*var)->getType(), 2);
        phi->addIncoming(*var, incomingbb);
        phi->addIncoming(previous[index], previousbb);
        *var = phi;
    }
}


Else::Else(If &head)
{
    head.previousbb = head.jit.builder.GetInsertBlock();

    auto *elsebb = head.nextbb;
    head.nextbb = head.jit.create_bb("if_out", elsebb->getParent());

    head.jit.builder.CreateBr(head.nextbb);
    head.jit.builder.SetInsertPoint(elsebb);

    for (int index = 0; index < std::ssize(head.variables); ++index) {
        std::swap(head.previous[index], *head.variables[index]);
    }
}

}

