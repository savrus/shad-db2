#include "jit.h"
#include "flow.h"
#include "answers.h"

using namespace shdb;
using llvm::CmpInst;

dummy_sgn answer_dummy(Jit &jit)
{
    // Your code goes here
    return {};
}

constant_sgn answer_constant(Jit &jit, int constant)
{
    // Your code goes here
    return {};
}

trivial_sgn answer_trivial(Jit &jit)
{
    // Your code goes here
    return {};
}

uintptr_t answer_sum(Jit &jit, int count)
{
    // Your code goes here
    return {};
}

call_sgn answer_call(Jit &jit)
{
    // Your code goes here
    return {};
}

abs_sgn answer_abs(Jit &jit)
{
    // Your code goes here
    return {};
}

for_sgn answer_for(Jit &jit)
{
    // Your code goes here
    return {};
}

