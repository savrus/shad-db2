#pragma once

#include "database.h"
#include "rowset.h"

namespace shdb {

struct Sql
{
    virtual ~Sql() = default;

    virtual Rowset execute(const std::string &query) = 0;
};

std::shared_ptr<Sql> create_sql(std::shared_ptr<Database> db);

}    // namespace shdb
