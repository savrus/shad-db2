#pragma once

#include "accessors.h"
#include "ast.h"
#include "jit.h"

namespace shdb {

struct Gear
{
    Gear *input = nullptr;
    Gear *output = nullptr;

    virtual ~Gear() = default;
    virtual void produce() = 0;
    virtual void consume(const JitRow &rowin) = 0;

    void insert_input(Gear *gear);
};

struct Output : public Gear
{
    Jit &jit;
    std::shared_ptr<RowsetAccessor> rowset_accessor;
    std::shared_ptr<RowsAccessor> rows_accessor;

    Output(Jit &jit, std::shared_ptr<RowsetAccessor> rowset_accessor, std::shared_ptr<RowsAccessor> rows_accessor);
    virtual void produce() override;
    virtual void consume(const JitRow &rowin) override;
};

// Your code goes here

}    // namespace shdb
