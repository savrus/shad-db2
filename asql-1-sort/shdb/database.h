#pragma once

#include "schema.h"
#include "rowset.h"

#include <memory>
#include <string>
#include <unordered_map>

namespace shdb {

class Database
{
    std::unordered_map<std::string, std::shared_ptr<Rowset>> tables;

public:
    void create_table(const std::string &name, std::shared_ptr<Schema> schema);
    std::shared_ptr<Rowset> get_table(const std::string &name);
    bool check_table_exists(const std::string &name);
    void drop_table(const std::string &name);
    std::shared_ptr<Schema> find_table_schema(const std::string &name);
};

std::shared_ptr<Database> connect();

}    // namespace shdb
