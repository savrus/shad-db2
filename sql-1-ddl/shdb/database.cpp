#include "database.h"

namespace shdb {

std::shared_ptr<Database> connect()
{
    static auto singleton = std::make_shared<Database>();
    return singleton;
}

void Database::create_table(const std::string &name, std::shared_ptr<Schema> schema)
{
    auto it = tables.emplace(name, std::make_shared<Rowset>(std::move(schema)));
    if (!it.second) {
        throw std::runtime_error("Table already exists.");
    }
}

std::shared_ptr<Rowset> Database::get_table(const std::string &name)
{
    return tables.at(name);
}

bool Database::check_table_exists(const std::string &name)
{
    return tables.contains(name);
}

void Database::drop_table(const std::string &name)
{
    tables.erase(name);
}

std::shared_ptr<Schema> Database::find_table_schema(const std::string &name)
{
    return tables.at(name)->schema;
}

}    // namespace shdb
