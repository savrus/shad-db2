#include "aggregate.h"

#include "flow.h"
#include "schema.h"

namespace shdb {

// Your code goes here.

Aggregate::Aggregate(Jit &jit, Type type) : jit(jit), type(type)
{}


Sum::Sum(Jit &jit, Type type) : Aggregate(jit, type)
{}

size_t Sum::get_state_size()
{
    std::abort();
}

void Sum::init(llvm::Value *state_ptr)
{
    std::abort();
}

void Sum::update(llvm::Value *state_ptr, const JitValue &value)
{
    std::abort();
}

JitValue Sum::finalize(llvm::Value *state_ptr)
{
    std::abort();
}

Min::Min(Jit &jit, Type type) : Aggregate(jit, type)
{
    std::abort();
}

size_t Min::get_state_size()
{
    std::abort();
}

void Min::init(llvm::Value *state_ptr)
{
    std::abort();
}

void Min::update(llvm::Value *state_ptr, const JitValue &value)
{
    std::abort();
}

JitValue Min::finalize(llvm::Value *state_ptr)
{
    std::abort();
}

Max::Max(Jit &jit, Type type) : Aggregate(jit, type)
{
    std::abort();
}

size_t Max::get_state_size()
{
    std::abort();
}

void Max::init(llvm::Value *state_ptr)
{
    std::abort();
}

void Max::update(llvm::Value *state_ptr, const JitValue &value)
{
    std::abort();
}

JitValue Max::finalize(llvm::Value *state_ptr)
{
    std::abort();
}

Avg::Avg(Jit &jit, Type type) : Aggregate(jit, type)
{
    std::abort();
}

size_t Avg::get_state_size()
{
    std::abort();
}

void Avg::init(llvm::Value *state_ptr)
{
    std::abort();
}

void Avg::update(llvm::Value *state_ptr, const JitValue &value)
{
    std::abort();
}

JitValue Avg::finalize(llvm::Value *state_ptr)
{
    std::abort();
}


std::unique_ptr<Aggregate> create_aggregate(Jit &jit, const std::string &name, Type type)
{
    if (name == "sum") {
        return std::make_unique<Sum>(jit, type);
    } else if (name == "min") {
        return std::make_unique<Min>(jit, type);
    } else if (name == "max") {
        return std::make_unique<Max>(jit, type);
    } else if (name == "avg") {
        return std::make_unique<Avg>(jit, type);
    } else {
        throw "Unknown function";
    }
}

}    // namespace shdb
